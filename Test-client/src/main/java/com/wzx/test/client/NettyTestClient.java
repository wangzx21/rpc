package com.wzx.test.client;

import com.wzx.rpc.api.HelloObject;
import com.wzx.rpc.api.HelloService;
import com.wzx.rpc.core.transport.RpcClient;
import com.wzx.rpc.core.transport.RpcClientProxy;
import com.wzx.rpc.core.transport.netty.client.NettyClient;

/**
 * @author wzx
 * @create 2021-12-07 22:24
 */
public class NettyTestClient {

    public static void main(String[] args) {
        RpcClient client = new NettyClient("127.0.0.1", 9999);
        RpcClientProxy rpcClientProxy = new RpcClientProxy(client);

        HelloService helloService = rpcClientProxy.getProxy(HelloService.class);
        HelloObject object = new HelloObject(12, "This is a message");
        String res = helloService.hello(object);
        System.out.println(res);

    }

}

