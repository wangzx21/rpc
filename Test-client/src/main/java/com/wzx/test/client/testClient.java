package com.wzx.test.client;

import com.wzx.rpc.api.HelloObject;
import com.wzx.rpc.api.HelloService;
import com.wzx.rpc.core.transport.RpcClient;
import com.wzx.rpc.core.transport.RpcClientProxy;
import com.wzx.rpc.core.transport.netty.client.NettyClient;

/**
 * @author wzx
 * @create 2021-12-07 14:33
 */
//使用动态代理,让代理对象帮助发送请求给服务端
public class testClient {
    public static void main(String[] args) {
        /*RpcClient client = new NettyClient("127.0.0.1", 9999);
        RpcClientProxy proxy = new RpcClientProxy(client);
        //获得代理
        HelloService helloService = proxy.getProxy(HelloService.class);
        HelloObject object = new HelloObject(999,"TEST MESSAGE");
        String res = helloService.hello(object);
        System.out.println(res);*/
    }
}
