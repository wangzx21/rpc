package com.wzx.rpc.core.registry;

/**
 * @author wzx
 * @create 2021-12-07 15:38
 */

import com.wzx.rpc.common.enumeration.RpcError;
import com.wzx.rpc.common.exception.RpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 我们将服务名与提供服务的对象的对应关系保存在一个 ConcurrentHashMap中，
 * 并且使用一个 Set 来保存当前有哪些对象已经被注册。
 * 在注册服务时，默认采用这个对象实现的接口的完整类名作为服务名，
 * 获得服务的对象就更简单了，直接去 Map 里查找就行了。
 */
public class ServiceProviderImpl implements ServiceProvider{
    private static final Logger logger = LoggerFactory.getLogger(ServiceProviderImpl.class);
    //static是为了保证全局唯一性
    //Map用来保存服务名和提供服务的对象的映射关系
    private static final Map<String,Object> serviceMap = new ConcurrentHashMap<String,Object>();
    //Set用来保存哪些服务已经被注册
    private static final Set<String> registeredService = ConcurrentHashMap.newKeySet();

    @Override
    public synchronized <T> void register(T service) {
        //注册服务,先获得服务的名字,默认使用完整类名作为服务名
        String serviceName = service.getClass().getCanonicalName();
        //如果该服务已经被注册过,直接返回
        if(registeredService.contains(serviceName)) return;
        registeredService.add(serviceName);
        //注册的服务肯定是一个对象,这一步是获得该对象实现的接口
        Class<?>[] interfaces = service.getClass().getInterfaces();
        if(interfaces.length == 0){
            throw new RpcException(RpcError.SERVICE_NOT_IMPLEMENT_ANY_INTERFACE);
        }
        for(Class<?> i : interfaces){
            serviceMap.put(i.getCanonicalName(), service);
            logger.info("向服务列表中加入了服务{},实现类为{}",i.getCanonicalName(),service);
        }
        logger.info("向接口: {} 注册服务: {}", interfaces, serviceName);
    }

    @Override
    public synchronized Object getService(String serviceName) {
        Object service = serviceMap.get(serviceName);
        if(service == null) {
            throw new RpcException(RpcError.SERVICE_NOT_FOUND);
        }
        return service;
    }
}
