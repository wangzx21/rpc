package com.wzx.rpc.core.transport.netty.client;

import com.wzx.rpc.common.entity.RpcRequest;
import com.wzx.rpc.common.entity.RpcResponse;
import com.wzx.rpc.core.codec.CommonDecoder;
import com.wzx.rpc.core.codec.CommonEncoder;
import com.wzx.rpc.core.registry.nacos.NacosServiceRegistry;
import com.wzx.rpc.core.registry.nacos.ServiceRegistry;
import com.wzx.rpc.core.serializer.JsonSerializer;
import com.wzx.rpc.core.serializer.kryo.KryoSerializer;
import com.wzx.rpc.core.transport.RpcClient;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.net.InterfaceAddress;

/**
 * @author wzx
 * @create 2021-12-07 18:50
 */
public class NettyClient implements RpcClient {

    private static final Logger logger = LoggerFactory.getLogger(NettyClient.class);
    private static ServiceRegistry serviceRegistry = new NacosServiceRegistry();
    private final String host;
    private int port;
    private static final Bootstrap bootstrap;

    public NettyClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    static {
        EventLoopGroup group = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new CommonDecoder())
                                .addLast(new CommonEncoder(new KryoSerializer()))
                                .addLast(new NettyClientHandler());
                    }
                });
    }

    @Override
    public Object sendRequest(RpcRequest rpcRequest) {
        try {
            //客户端发送请求时先从注册中心中找到服务对应的服务提供者的IP地址和端口号
            InetSocketAddress inetSocketAddress = serviceRegistry.lookupService(rpcRequest.getInterfaceName());
            if(inetSocketAddress == null)   logger.error("未找到服务{}对应的提供者",rpcRequest.getInterfaceName());
            logger.info("在nacos中对应服务 {}的ip地址为: {}",rpcRequest.getInterfaceName(),inetSocketAddress.getAddress());
            ChannelFuture future = bootstrap.connect(inetSocketAddress).sync();
            logger.info("客户端连接到服务器 {}:{}", inetSocketAddress.getHostName() ,inetSocketAddress.getAddress());
            //建立管道连接
            Channel channel = future.channel();
            if(channel != null) {
                channel.writeAndFlush(rpcRequest).addListener(future1 -> {
                    if(future1.isSuccess()) {
                        logger.info(String.format("客户端发送消息: %s", rpcRequest));
                    } else {
                        logger.error("发送消息时有错误发生: ", future1.cause());
                    }
                });
                channel.closeFuture().sync();
                AttributeKey<RpcResponse> key = AttributeKey.valueOf("rpcResponse");
                RpcResponse rpcResponse = channel.attr(key).get();
                return rpcResponse;
            }
        } catch (InterruptedException e) {
            logger.error("发送消息时有错误发生: ", e);
        }
        return null;
    }

    @Override
    public Object sendRequest(RpcRequest rpcRequest, String host, int port) {
        return null;
    }
}
