package com.wzx.rpc.core.registry;

/**
 * @author wzx
 * @create 2021-12-08 17:46
 */

/**
 * ServiceProvider是用于保存服务名和对应的实际实现类
 */
public interface ServiceProvider {
    //register用于注册服务信息
    <T> void register(T service);
    //getService用于获取服务信息
    Object getService(String serviceName);
}
