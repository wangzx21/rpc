package com.wzx.rpc.core.transport;

/**
 * @author wzx
 * @create 2021-12-07 13:46
 */

import com.wzx.rpc.common.entity.RpcRequest;
import com.wzx.rpc.common.entity.RpcResponse;
import com.wzx.rpc.core.transport.netty.client.NettyClient;
import com.wzx.rpc.core.transport.socket.SocketClient;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * RPC客户端动态代理
 */
public class RpcClientProxy implements InvocationHandler {
    private String host = "127.0.0.1";
    private int port = 9999;
    private RpcClient rpcClient;
    public RpcClientProxy(String host, int port) {
        this.host = host;
        this.port = port;
    }
    public RpcClientProxy(RpcClient rpcClient){
        this.rpcClient = rpcClient;
    }

    @SuppressWarnings("unchecked")
    public <T> T getProxy(Class<T> clazz){
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(),new Class<?>[]{clazz},this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //这里使用builder()方法来生成这个对象（RpcRequest类里有Builder注解)
        //通过动态代理将请求发给服务端,并接受服务端响应的数据
        RpcRequest rpcRequest = RpcRequest.builder()
                .interfaceName(method.getDeclaringClass().getName())
                .methodName(method.getName())
                .parameters(args)
                .paramTypes(method.getParameterTypes())
                .build();
        //SocketClient rpcClient = new SocketClient();

        return ((RpcResponse) rpcClient.sendRequest(rpcRequest)).getData();
    }
}
