package com.wzx.rpc.core.handler;

import com.wzx.rpc.common.entity.RpcRequest;
import com.wzx.rpc.common.entity.RpcResponse;
import com.wzx.rpc.core.registry.ServiceProvider;
import com.wzx.rpc.core.registry.nacos.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * @author wzx
 * @create 2021-12-07 18:02
 */
public class RequestHandlerThread implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(RequestHandlerThread.class);

    private Socket socket;
    private RequestHandler requestHandler;
    private ServiceProvider serviceProvider;

    public RequestHandlerThread(Socket socket, RequestHandler requestHandler, ServiceProvider serviceProvider) {
        this.socket = socket;
        this.requestHandler = requestHandler;
        this.serviceProvider = serviceProvider;
    }

    @Override
    public void run() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream())) {
            //读取请求数据
            RpcRequest rpcRequest = (RpcRequest) objectInputStream.readObject();
            //获得请求方法实现的接口名
            String interfaceName = rpcRequest.getInterfaceName();
            //从服务注册表中找出对应的服务实现类
            Object service = serviceProvider.getService(interfaceName);
            //由requestHandler负责处理
            Object result = requestHandler.handle(rpcRequest, service);
            objectOutputStream.writeObject(RpcResponse.success(result));
            objectOutputStream.flush();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("调用或发送时有错误发生：", e);
        }
    }
}

