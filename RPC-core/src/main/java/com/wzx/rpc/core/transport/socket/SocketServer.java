package com.wzx.rpc.core.transport.socket;

/**
 * @author wzx
 * @create 2021-12-07 13:46
 */


import com.wzx.rpc.core.handler.RequestHandler;
import com.wzx.rpc.core.handler.RequestHandlerThread;
import com.wzx.rpc.core.registry.ServiceProvider;
import com.wzx.rpc.core.registry.nacos.ServiceRegistry;
import com.wzx.rpc.core.transport.RpcServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/**
 * 完成了服务注册，我们的服务端也需要稍作修改
 * 为了降低耦合度，我们不会把 ServiceRegistry 和某一个 RpcServer 绑定在一起，
 * 而是在创建 RpcServer 对象时，传入一个 ServiceRegistry 作为这个服务的注册表
 */
public class SocketServer implements RpcServer {

    private static final Logger logger = LoggerFactory.getLogger(SocketServer.class);
    private int port;
    private static final int CORE_POOL_SIZE = 5;
    private static final int MAXIMUM_POOL_SIZE = 50;
    private static final int KEEP_ALIVE_TIME = 60;
    private static final int BLOCKING_QUEUE_CAPACITY = 100;
    private final ExecutorService threadPool;
    private RequestHandler requestHandler = new RequestHandler();
    private final ServiceProvider serviceProvider;


    public SocketServer(ServiceProvider serviceProvider) {
        //创建RpcServer时要绑定对应的服务注册表
        this.serviceProvider = serviceProvider;
        BlockingQueue<Runnable> workingQueue = new ArrayBlockingQueue<>(BLOCKING_QUEUE_CAPACITY);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        threadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, workingQueue, threadFactory);

    }

    @Override
    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            logger.info("服务器启动......");
            Socket socket;
            while((socket = serverSocket.accept()) != null) {
                //start请求仅负责启动服务端并监听客户端的请求
                logger.info("消费者连接: {}:{}", socket.getInetAddress(), socket.getPort());
                //在线程池启动一个线程负责真正的处理
                /**
                 * 在每一个请求处理线程（RequestHandlerThread）中也就需要传入 ServiceRegistry 了，这里把处理线程和处理逻辑分成了两个类：
                 * RequestHandlerThread 只是一个线程，从ServiceRegistry 获取到提供服务的对象后，就会把 RpcRequest 和服务对象直接交给 RequestHandler 去处理，反射等过程被放到了 RequestHandler 里。
                 *
                 */
                threadPool.execute(new RequestHandlerThread(socket, requestHandler, serviceProvider));
            }
            threadPool.shutdown();
        } catch (IOException e) {
            logger.error("服务器启动时有错误发生:", e);
        }
    }

    @Override
    public <T> void publishService(T service, String serviceName) {

    }





    /*这里简化了一下，RpcServer暂时只能注册一个接口，
    即对外提供一个接口的调用服务，添加register方法，
    在注册完一个服务后立刻开始监听
    */
    /*//TODO 注册服务后不自动启动服务器,将注册服务与服务器的启动分离开
    public void register(Object service, int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            logger.info("服务器正在启动...");
            Socket socket;
            while((socket = serverSocket.accept()) != null) {
                logger.info("客户端连接！Ip为：" + socket.getInetAddress());
                //传入对应的socket和service,获得客户端要调用的service
                threadPool.execute(new WorkerThread(socket, service));
            }
        } catch (IOException e) {
            logger.error("连接时有错误发生：", e);
        }
    }*/
    /*
    这是个内部类，是我们的工作线程
    这里向工作线程WorkerThread传入了socket和用于服务端实例service。
    WorkerThread实现了Runnable接口，用于接收RpcRequest对象，
    解析并且调用，生成RpcResponse对象并传输回去
    */
    /*private class WorkerThread implements Runnable {
        private Socket socket;
        private Object service;
        public WorkerThread(Socket socket, Object service) {
            this.socket = socket;
            this.service = service;
        }

        @Override
        public void run() {
            try (ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                 ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream())) {
                RpcRequest rpcRequest = (RpcRequest) objectInputStream.readObject();
                //通过请求的方法名和方法的参数类型获得要调用的方法
                Method method = service.getClass().getMethod(rpcRequest.getMethodName(), rpcRequest.getParamTypes());
                //调用对应的方法
                Object returnObject = method.invoke(service, rpcRequest.getParameters());
                //将调用后得到的返回值写入输出流中
                objectOutputStream.writeObject(RpcResponse.success(returnObject));
                //清空缓存
                objectOutputStream.flush();
            } catch (IOException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                logger.error("调用或发送时有错误发生：", e);
            }
        }
    }*/
}

