package com.wzx.rpc.core.registry.nacos;

/**
 * @author wzx
 * @create 2021-12-07 15:34
 */

import com.wzx.rpc.core.loadBalancer.LoadBalancer;

import java.net.InetSocketAddress;

/**
 * ServiceRegistry通过服务名获得服务提供者的ip地址
 */
public interface ServiceRegistry {
    //将服务的名称和地址注册进服务注册中心
    void register(String serviceName, InetSocketAddress inetSocketAddress);
    //根据服务名称从注册中心获取到一个服务提供者的地址
    InetSocketAddress lookupService(String serviceName);
    //为服务中心选择负载均衡策略
    void setLoadBalancer(LoadBalancer loadBalancer);
}

