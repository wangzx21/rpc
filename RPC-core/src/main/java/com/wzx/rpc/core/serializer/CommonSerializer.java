package com.wzx.rpc.core.serializer;

import com.wzx.rpc.core.serializer.kryo.KryoSerializer;

/**
 * @author wzx
 * @create 2021-12-07 18:49
 */
public interface CommonSerializer {

    //获得序列化器的code
    public int getCode();
    //对数据进行序列化
    public byte[] serialize(Object obj);
    //对数据进行反序列化
    public Object deserialize(byte[] bytes,Class<?> clazz);

    static CommonSerializer getByCode(int code) {
        switch (code) {
            case 1:
                return new JsonSerializer();
            case 0:
                return new KryoSerializer();
            default:
                return null;
        }
    }

}
