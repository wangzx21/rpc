package com.wzx.rpc.core.annotation;

import java.lang.annotation.*;

/**
 * @author wzx
 * @create 2021-12-06 17:45
 */

/**
 * 表示被注解的方法是一个可以提供远程服务的方法
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RPCService {
    //被注解的方法是一个可以提供远程调用的服务
    String name() default "";
}
