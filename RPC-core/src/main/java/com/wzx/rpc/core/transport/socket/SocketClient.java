package com.wzx.rpc.core.transport.socket;

/**
 * @author wzx
 * @create 2021-12-07 13:46
 */

import com.wzx.rpc.common.entity.RpcRequest;
import com.wzx.rpc.core.transport.RpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *发送的逻辑我使用了一个RpcClient对象来实现
 这个对象的作用，就是将一个对象发过去，并且接受返回的对象
 */
public class SocketClient implements RpcClient {
    private static final Logger logger = LoggerFactory.getLogger(SocketClient.class);

    @Override
    public Object sendRequest(RpcRequest rpcRequest) {
        return null;
    }

    @Override
    public Object sendRequest(RpcRequest rpcRequest, String host, int port) {
        try (Socket socket = new Socket(host, port)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(rpcRequest);
            //objectOutputStream.flush();
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("调用时有错误发生：", e);
            return null;
        }
    }
}

