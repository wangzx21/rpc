package com.wzx.rpc.core.transport;

/**
 * @author wzx
 * @create 2021-12-07 21:58
 */
public interface RpcServer {
    void start();
    //注册服务
    <T> void publishService(T service, String serviceName);
}
