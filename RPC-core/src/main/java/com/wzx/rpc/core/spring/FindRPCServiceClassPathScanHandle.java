package com.wzx.rpc.core.spring;

import com.wzx.rpc.core.annotation.RPCService;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.util.Set;

/**
 * @author wzx
 * @create 2021-12-06 17:57
 */
//FindRpcServiceClassPathScanHandle类是我们自定义的包扫描器，我们可以在这个扫描器中添加我们的过滤条件。
public class FindRPCServiceClassPathScanHandle extends ClassPathBeanDefinitionScanner {

    public FindRPCServiceClassPathScanHandle(BeanDefinitionRegistry registry, boolean useDefaultFilters) {
        super(registry, useDefaultFilters);
    }

    @Override
    protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
        //添加过滤条件，这里是只添加了@RPCService的注解才会被扫描到,IncludeFilter指包括某个过滤器
        addIncludeFilter(new AnnotationTypeFilter(RPCService.class));
        //调用spring的扫描
        Set<BeanDefinitionHolder> beanDefinitionHolders = super.doScan(basePackages);
        return beanDefinitionHolders;
    }
}
