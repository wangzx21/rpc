package com.wzx.rpc.core.registry.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.wzx.rpc.common.enumeration.RpcError;
import com.wzx.rpc.common.exception.RpcException;
import com.wzx.rpc.core.loadBalancer.LoadBalancer;
import com.wzx.rpc.core.loadBalancer.RandomLoadBalancer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * @author wzx
 * @create 2021-12-08 17:50
 */
public class NacosServiceRegistry implements ServiceRegistry {
    private static final Logger logger = LoggerFactory.getLogger(NacosServiceRegistry.class);
    private static final String SERVER_ADDR = "127.0.0.1:8848";
    private static final NamingService namingService;

    private static LoadBalancer LOAD_BALANCER = new RandomLoadBalancer();

    static {
        try {
            //通过 NamingFactory 创建 NamingService 连接 Nacos
            namingService = NamingFactory.createNamingService(SERVER_ADDR);
        } catch (NacosException e) {
            logger.error("连接到Nacos时有错误发生: ", e);
            throw new RpcException(RpcError.FAILED_TO_CONNECT_TO_SERVICE_REGISTRY);
        }
    }

    @Override
    public void register(String serviceName, InetSocketAddress inetSocketAddress) {
        try {
            namingService.registerInstance(serviceName, inetSocketAddress.getHostName(), inetSocketAddress.getPort());
        } catch (NacosException e) {
            logger.error("注册服务时有错误发生:", e);
            throw new RpcException(RpcError.REGISTER_SERVICE_FAILED);
        }
    }

    @Override
    public InetSocketAddress lookupService(String serviceName) {
        try {
            //使用随机算法
            List<Instance> instances = namingService.getAllInstances(serviceName);
            Instance instance = LOAD_BALANCER.select(instances);
            return new InetSocketAddress(instance.getIp(), instance.getPort());
        } catch (NacosException e) {
            logger.error("获取服务时有错误发生:", e);
        }
        return null;
    }

    @Override
    public void setLoadBalancer(LoadBalancer loadBalancer) {
        LOAD_BALANCER = loadBalancer;
    }

}
