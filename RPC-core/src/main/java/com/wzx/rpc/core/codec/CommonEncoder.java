package com.wzx.rpc.core.codec;
import com.wzx.rpc.common.entity.RpcRequest;
import com.wzx.rpc.common.enumeration.PackageType;
import com.wzx.rpc.core.serializer.CommonSerializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author wzx
 * @create 2021-12-07 18:47
 */

/**
 * 负责编码,在原本的数据上加上各种必要的数据形成自定义的协议
 *                          定义协议字段如下
 * +---------------+---------------+-----------------+-------------+
 * |  Magic Number |  Package Type | Serializer Type | Data Length |
 * |    4 bytes    |    4 bytes    |     4 bytes     |   4 bytes   |
 * +---------------+---------------+-----------------+-------------+
 * |                          Data Bytes                           |
 * |                   Length: ${Data Length}                      |
 * +---------------------------------------------------------------+
 * Magic Number标识一个协议包
 * Package Type表示这是一个调用请求还是一个调用响应
 * Serializer Type指明了实际数据使用的序列化器
 * Data Length表示实际数据的长度
 *
 * PS:自定义协议中魔数的作用:
 * 快速 识别字节流是否是程序能够处理的，能处理才进行后面的 耗时 业务操作，如果不能处理，尽快执行失败，断开连接等操作。
 */
public class CommonEncoder extends MessageToByteEncoder {
    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    private final CommonSerializer serializer;

    //传入一个序列化器进行初始化
    public CommonEncoder(CommonSerializer serializer) {
        this.serializer = serializer;
    }

    //msg是要进行编码的数据
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object msg, ByteBuf out) throws Exception {
        //发送魔数
        out.writeInt(MAGIC_NUMBER);
        //发送Package Type
        if(msg instanceof RpcRequest){
            out.writeInt(PackageType.REQUEST_PACK.getCode());
        }else {
            out.writeInt(PackageType.RESPONSE_PACK.getCode());
        }
        //发送Serializer Type
        out.writeInt(serializer.getCode());
        //对数据进行序列化
        byte[] bytes = serializer.serialize(msg);
        out.writeInt(bytes.length);
        out.writeBytes(bytes);
    }
}
