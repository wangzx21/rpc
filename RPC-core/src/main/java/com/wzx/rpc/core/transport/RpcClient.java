package com.wzx.rpc.core.transport;

import com.wzx.rpc.common.entity.RpcRequest;

/**
 * @author wzx
 * @create 2021-12-07 21:58
 */
public interface RpcClient {
    Object sendRequest(RpcRequest rpcRequest);
    Object sendRequest(RpcRequest rpcRequest,String host,int port);
}
