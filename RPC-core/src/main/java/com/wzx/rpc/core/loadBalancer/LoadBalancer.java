package com.wzx.rpc.core.loadBalancer;

import com.alibaba.nacos.api.naming.pojo.Instance;

import java.util.List;

/**
 * @author wzx
 * @create 2021-12-09 19:21
 */
public interface LoadBalancer {
    public Instance select(List<Instance> instances);
}
