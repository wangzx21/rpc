package com.wzx.rpc.core.codec;

import com.wzx.rpc.common.entity.RpcRequest;
import com.wzx.rpc.common.entity.RpcResponse;
import com.wzx.rpc.common.enumeration.PackageType;
import com.wzx.rpc.common.enumeration.RpcError;
import com.wzx.rpc.common.exception.RpcException;
import com.wzx.rpc.core.serializer.CommonSerializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author wzx
 * @create 2021-12-07 18:48
 */

/**
 * 负责编码,在原本的数据上加上各种必要的数据形成自定义的协议
 *                          定义协议字段如下
 * +---------------+---------------+-----------------+-------------+
 * |  Magic Number |  Package Type | Serializer Type | Data Length |
 * |    4 bytes    |    4 bytes    |     4 bytes     |   4 bytes   |
 * +---------------+---------------+-----------------+-------------+
 * |                          Data Bytes                           |
 * |                   Length: ${Data Length}                      |
 * +---------------------------------------------------------------+
 * Magic Number标识一个协议包
 * Package Type表示这是一个调用请求还是一个调用响应
 * Serializer Type指明了实际数据使用的序列化器
 * Data Length表示实际数据的长度
 *
 * PS:自定义协议中魔数的作用:
 * 快速 识别字节流是否是程序能够处理的，能处理才进行后面的 耗时 业务操作，如果不能处理，尽快执行失败，断开连接等操作。
 */
public class CommonDecoder extends ByteToMessageDecoder {
    private static final Logger logger = LoggerFactory.getLogger(CommonDecoder.class);
    private static final int MAGIC_NUMBER = 0xCAFEBABE;

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> out) throws Exception {
        //先判断魔数是否一致
        int magic = in.readInt();
        if(magic != MAGIC_NUMBER){
            logger.error("不能识别的数据包!!:{}",magic);
            throw new RpcException(RpcError.UNKNOWN_PROTOCOL);
        }
        //调用类型,是请求还是响应
        int packageCode = in.readInt();
        Class<?> packageClass;
        if(packageCode == PackageType.REQUEST_PACK.getCode()){
            packageClass = RpcRequest.class;
        }else if(packageCode == PackageType.RESPONSE_PACK.getCode()){
            packageClass = RpcResponse.class;
        }else{
            logger.error("不识别的数据包!!:{}",packageCode);
            throw new RpcException(RpcError.UNKNOWN_PACKAGE_TYPE);
        }
        //序列化的类型
        int serializerCode = in.readInt();
        //判断序列化器是否可识别
        CommonSerializer serializer = CommonSerializer.getByCode(serializerCode);
        if(serializer == null){
            logger.error("不识别的反序列化器!!:{}",serializerCode);
            throw new RpcException(RpcError.UNKNOWN_SERIALIZER);
        }
        //读取数据
        int length = in.readInt();
        if(length > in.readableBytes()){
            logger.error("告警：读到的消息体长度小于传送过来的消息长度!");
            throw new RpcException(RpcError.PACKAGE_LENGTH);
        }
        byte[] bytes = new byte[length];
        in.readBytes(bytes);
        //反序列化
        Object obj = serializer.deserialize(bytes,packageClass);
        out.add(obj);
    }
}
