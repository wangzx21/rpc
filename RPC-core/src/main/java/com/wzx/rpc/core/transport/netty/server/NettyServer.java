package com.wzx.rpc.core.transport.netty.server;

import com.wzx.rpc.common.enumeration.RpcError;
import com.wzx.rpc.common.exception.RpcException;
import com.wzx.rpc.common.util.ReflectUtil;
import com.wzx.rpc.core.annotation.RPCService;
import com.wzx.rpc.core.annotation.RPCServiceScan;
import com.wzx.rpc.core.codec.CommonDecoder;
import com.wzx.rpc.core.codec.CommonEncoder;
import com.wzx.rpc.core.hook.ShutdownHook;
import com.wzx.rpc.core.registry.ServiceProvider;
import com.wzx.rpc.core.registry.ServiceProviderImpl;
import com.wzx.rpc.core.registry.nacos.ServiceRegistry;
import com.wzx.rpc.core.registry.nacos.NacosServiceRegistry;
import com.wzx.rpc.core.serializer.kryo.KryoSerializer;
import com.wzx.rpc.core.transport.RpcServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Set;

/**
 * @author wzx
 * @create 2021-12-07 18:50
 */
public class NettyServer implements RpcServer {
    private final String host;
    private final int port;
    private final ServiceRegistry serviceRegistry;
    private final ServiceProvider serviceProvider;

    public NettyServer(String host, int port) {
        this.host = host;
        this.port = port;
        serviceRegistry = new NacosServiceRegistry();
        serviceProvider = new ServiceProviderImpl();
        scanServices();
    }


    private static final Logger logger = LoggerFactory.getLogger(NettyServer.class);

    @Override
    public void start() {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();

            ShutdownHook.getShutdownHook().addClearAllHook();
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .option(ChannelOption.SO_BACKLOG, 256)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new CommonEncoder(new KryoSerializer()));
                            pipeline.addLast(new CommonDecoder());
                            pipeline.addLast(new NettyServerHandler());
                        }
                    });
            ChannelFuture future = serverBootstrap.bind(host, port).sync();
            future.channel().closeFuture().sync();

        } catch (InterruptedException e) {
            logger.error("启动服务器时有错误发生: ", e);
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    @Override
    public <T> void publishService(T service, String serviceName) {
        //发布服务
        serviceProvider.register(service);
        serviceRegistry.register(serviceName,new InetSocketAddress(host,port));
        //start();
    }
    //自动扫描服务并注册
    public void scanServices() {
        String mainClassName = ReflectUtil.getStackTrace();
        Class<?> startClass;
        try {
            startClass = Class.forName(mainClassName);
            //通过isAnnotationPresent方法见检查startClass类上是否有RPCServiceScan注解
            if(!startClass.isAnnotationPresent(RPCServiceScan.class)) {
                logger.error("启动类缺少 @ServiceScan 注解");
                throw new RpcException(RpcError.SERVICE_SCAN_PACKAGE_NOT_FOUND);
            }
        } catch (ClassNotFoundException e) {
            logger.error("出现未知错误");
            throw new RpcException(RpcError.UNKNOWN_ERROR);
        }
        //获取扫描的包路径
        String[] basePackages = startClass.getAnnotation(RPCServiceScan.class).basePackage();
        for(String basePackage : basePackages){
            if("".equals(basePackage)) {
                //如果扫描路径为空,默认为扫描当前类所在包下的所有方法
                basePackage = mainClassName.substring(0, mainClassName.lastIndexOf("."));
            }
            //获得包路径下的所有类
            Set<Class<?>> classSet = ReflectUtil.getClasses(basePackage);
            for(Class<?> clazz : classSet) {
                if(clazz.isAnnotationPresent(RPCService.class)) {
                    String serviceName = clazz.getAnnotation(RPCService.class).name();
                    Object obj;
                    try {
                        obj = clazz.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        logger.error("创建 " + clazz + " 时有错误发生");
                        continue;
                    }
                    if("".equals(serviceName)) {
                        Class<?>[] interfaces = clazz.getInterfaces();
                        for (Class<?> oneInterface: interfaces){
                            publishService(obj, oneInterface.getCanonicalName());
                        }
                    } else {
                        publishService(obj, serviceName);
                    }
                }
            }
        }

    }

}

