package com.wzx.rpc.core.loadBalancer;

import com.alibaba.nacos.api.naming.pojo.Instance;

import java.util.List;

/**
 * @author wzx
 * @create 2021-12-09 19:23
 */

/**
 * 轮询算法
 */
public class RoundRobinLoadBalancer implements LoadBalancer{

    private int index = 0;
    @Override
    public Instance select(List<Instance> instances) {
        if(index >= instances.size()){
            index %= instances.size();
        }
        return instances.get(index++);
    }
}
