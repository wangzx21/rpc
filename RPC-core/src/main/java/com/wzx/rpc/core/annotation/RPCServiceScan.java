package com.wzx.rpc.core.annotation;

import org.springframework.context.annotation.Import;
import com.wzx.rpc.core.spring.RPCScannerRegistrar;

import java.lang.annotation.*;

/**
 * @author wzx
 * @create 2021-12-06 17:47
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
//spring中的注解,加载对应的类
@Import(RPCScannerRegistrar.class)//这个是我们的关键，实际上也是由这个类来扫描的
@Documented
public @interface RPCServiceScan {
    //指定哪些包下的方法将被扫描
    String[] basePackage() default "";
}
