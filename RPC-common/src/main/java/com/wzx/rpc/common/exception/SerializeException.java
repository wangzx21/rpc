package com.wzx.rpc.common.exception;

/**
 * @author wzx
 * @create 2021-12-08 17:22
 */
public class SerializeException extends RuntimeException{
    public SerializeException(String msg){super(msg);}
}
