package com.wzx.rpc.common.exception;

import com.wzx.rpc.common.enumeration.RpcError;

/**
 * @author wzx
 * @create 2021-12-07 13:18
 */
public class RpcException extends RuntimeException {
    public RpcException(RpcError rpcErrorMessageEnum, String detail) {
        super(rpcErrorMessageEnum.getMessage() + ":" + detail);
    }

    public RpcException(String message, Throwable cause) {
        super(message, cause);
    }

    public RpcException(RpcError rpcErrorMessageEnum) {
        super(rpcErrorMessageEnum.getMessage());
    }
}
