package com.wzx.rpc.common.enumeration;

/**
 * @author wzx
 * @create 2021-12-07 18:54
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 表示这是一个调用请求还是一个调用响应
 */
@AllArgsConstructor
@Getter
public enum PackageType {

    REQUEST_PACK(0),
    RESPONSE_PACK(1);

    private final int code;
}
