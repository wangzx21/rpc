package com.wzx.rpc.common.entity;

/**
 * @author wzx
 * @create 2021-12-07 13:17
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 客户端调用请求时传输的实际请求,服务端通过客户端传输的参数得到客户端要调用的是哪个方法
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RpcRequest implements Serializable {
    /**
     * 待调用接口名称
     */
    private String interfaceName;
    /**
     * 待调用方法名称
     */
    private String methodName;
    /**
     * 调用方法的参数
     */
    private Object[] parameters;
    /**
     * 调用方法的参数类型,与上面parameters数组中的元素相对应,主要是为了用于防止反序列化失败
     */
    private Class<?>[] paramTypes;
}


