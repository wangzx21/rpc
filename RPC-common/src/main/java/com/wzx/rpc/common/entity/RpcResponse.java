package com.wzx.rpc.common.entity;

/**
 * @author wzx
 * @create 2021-12-07 13:17
 */

import com.wzx.rpc.common.enumeration.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 服务端返回给客户端的信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RpcResponse<T> implements Serializable {
    /**
     * 响应状态码
     */
    private Integer statusCode;
    /**
     * 响应状态补充信息
     */
    private String message;
    /**
     * 响应数据
     */
    private T data;

    public static <T> RpcResponse<T> success(T data){
        RpcResponse<T> rpcResponse = new RpcResponse<>();
        rpcResponse.setStatusCode(ResponseCode.SUCCESS.getCode());
        rpcResponse.setData(data);
        return rpcResponse;
    }
    public static <T> RpcResponse<T> fail(ResponseCode code){
        RpcResponse<T> rpcResponse = new RpcResponse<>();
        rpcResponse.setStatusCode(ResponseCode.FAIL.getCode());
        rpcResponse.setMessage(code.getMessage());
        return rpcResponse;
    }

}
