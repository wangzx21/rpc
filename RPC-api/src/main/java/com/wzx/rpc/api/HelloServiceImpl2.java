package com.wzx.rpc.api;

/**
 * @author wzx
 * @create 2021-12-10 17:08
 */
public class HelloServiceImpl2 implements HelloService2{
    @Override
    public String hello2(HelloObject helloObject) {
        return "这是helloService2调用的返回值"+helloObject.getMessage();
    }
}
