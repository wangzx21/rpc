package com.wzx.rpc.api;

/**
 * @author wzx
 * @create 2021-12-07 13:12
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 测试api调用的实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//这三个注解是lombok的注解，自动创捷set get constructor的，
public class HelloObject implements Serializable {
    private Integer id;
    private String message;
}

