package com.wzx.rpc.api;

import com.wzx.rpc.core.annotation.RPCService;

/**
 * @author wzx
 * @create 2021-12-10 17:07
 */
public interface HelloService2 {
    String hello2(HelloObject helloObject);
}
