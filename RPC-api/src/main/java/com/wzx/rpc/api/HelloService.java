package com.wzx.rpc.api;

/**
 * @author wzx
 * @create 2021-12-07 13:12
 */

import com.wzx.rpc.core.annotation.RPCService;

/**
 * 服务端和客户端之间的接口
 */

public interface HelloService {
    String hello(HelloObject object);
}
