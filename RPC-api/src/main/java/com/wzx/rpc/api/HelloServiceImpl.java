package com.wzx.rpc.api;

import com.wzx.rpc.core.annotation.RPCService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author wzx
 * @create 2021-12-07 13:14
 */
/**
 * 服务端实现接口,接受客户端调用请求并将调用的结果返回给客户端
 */
@RPCService
public class HelloServiceImpl implements HelloService {
    private static final Logger logger = LoggerFactory.getLogger(HelloServiceImpl.class);
    @Override
    public String hello(HelloObject object) {
        logger.info("服务端接收到：{}", object.getMessage());
        return "这是调用的返回值，id=" + object.getId();
    }
}

