package com.wzx.test.server;

import com.wzx.rpc.api.HelloService;
import com.wzx.rpc.api.HelloServiceImpl;
import com.wzx.rpc.core.annotation.RPCService;
import com.wzx.rpc.core.annotation.RPCServiceScan;
import com.wzx.rpc.core.registry.ServiceProvider;
import com.wzx.rpc.core.registry.ServiceProviderImpl;
import com.wzx.rpc.core.registry.nacos.NacosServiceRegistry;
import com.wzx.rpc.core.registry.nacos.ServiceRegistry;
import com.wzx.rpc.core.transport.netty.server.NettyServer;

/**
 * @author wzx
 * @create 2021-12-07 22:24
 */
@RPCServiceScan(basePackage = {"com.wzx.test.serviceImpl","com.wzx.rpc.api"})
public class NettyTestServer {
    public static void main(String[] args) {
        //HelloService helloService = new HelloServiceImpl();
        NettyServer server = new NettyServer("127.0.0.1",9999);
        server.start();
    }
}

