package com.wzx.test.serviceImpl;

import com.wzx.rpc.api.HelloObject;
import com.wzx.rpc.api.HelloService2;
import com.wzx.rpc.core.annotation.RPCService;

/**
 * @author wzx
 * @create 2021-12-10 17:08
 */
@RPCService
public class HelloServiceImpl2 implements HelloService2 {
    @Override
    public String hello2(HelloObject helloObject) {
        return "这是helloService2调用的返回值"+helloObject.getMessage();
    }
}
